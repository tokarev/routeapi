/**
 * Route.js
 *
 * @description :: Model for routes
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

const uuid = require('uuid');
const crypto = require('crypto');
const Joi = require('joi');

const STATUS_IN_PROGRESS = 0;
const STATUS_DONE = 1;
const STATUS_FAILURE = 2;

module.exports = {
  attributes: {
    hash : {
      type: 'string',
      required: true,
      index: true
    },
    token : {
      type: 'uuid',
      defaultsTo: () => uuid.v4(),
      required: true,
      uuidv4: true,
      index: true
    },
    input :  {
      type: 'json'
    },
    result : {
      type: 'json'
    },
    status: {
      type: 'integer',
      defaultsTo: 0
    }
  },

  get(routeLegs, callback) {
    let err = null;
    const hash = crypto.createHash('md5').update(JSON.stringify(routeLegs)).digest('hex');
    let result = {};
    const schemaCoord = Joi.array().length(2).items(Joi.string().regex(/\d+\.\d+/));
    const schema = Joi.array().items(schemaCoord).min(2);
    const validationResult = Joi.validate(routeLegs, schema);

    if (!validationResult.error) {
      Route.findOrCreate(
        {hash},
        {
          hash,
          input: routeLegs,
          result
        },
        (err, record) => {
          if (err) {
            onvoya.log.error(err);
            return callback(err, result);
          } else {
            result = {
              token: record.token
            };
            if (record.status ==! STATUS_DONE || record.status ==! STATUS_FAILURE) {
              directions.get(routeLegs, (err, result) => {
                record.result = result;
                record.status = err ? STATUS_FAILURE : STATUS_DONE;
                record.save();
                sails.log.info(record);
              });
            }
            return callback(err, result);
          }
        });
    } else {
      err = "Wrong input parameters";
      return callback(err, result);
    }
  },

  getByToken(token, callback) {
    let err = null;
    let result = {};
    const validationResult = Joi.validate(token, Joi.string().uuid());
    if (!validationResult.error) {
      Route.findOne({token}, (err, record) => {
        if (err) {
          sails.log.error(err)
        };
        result.status = Route.getStatusText(record && record.status || 0);
        if (record) {
          if (record.status === STATUS_FAILURE) {
            result = {
              status: "failure",
              error: record && record.result && record.result.error
            }
          } else if (record.status === STATUS_DONE) {
            result.path = record.result.path;
            result.total_distance = record.result.total_distance;
            result.total_time = record.result.total_time;
          }
        } else {
          err = 'Invalid token';
        }
        return callback(err, result);
      })
    } else {
      err = 'Invalid token';
      return callback(err, result);
    }
  },

  getStatusText(status) {
    let statusText = '';
    switch (status) {
      case STATUS_DONE:
        statusText = 'success';
        break;
      case STATUS_FAILURE:
        statusText = 'failure';;
        break;
      default:
        statusText = 'in progress';
    }
    return statusText;
  }
};
