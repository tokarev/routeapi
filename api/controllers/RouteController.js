/**
 * RouteController
 *
 * @description :: controller for Route API
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  makeRequest(req, res) {
    const routeLegs = req.body;
    Route.get(routeLegs, (err, result) => {
      if (err) {
        sails.log.info(err);
        return res.json({"error": err});
      }
      return res.json(result);
    });
  },

  getResult(req, res) {
    const token = req.param('token');
    Route.getByToken(token, (err, result) => {
      if (err) {
        sails.log.info(err);
        return res.json({"error": err});
      }
      return res.json(result);
    });
  }

};
