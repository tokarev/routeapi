const googleMapsClient = require('@google/maps').createClient({
  key: sails.config.keys.google.directions
});
const Joi = require('joi');
// @todo make timeout error in case google directions api doesn't respond for some time
module.exports = {
  get: (waypoints, callback) => {
    sails.log.info('googleMapsClient.directions request started', waypoints);

    googleMapsClient.distanceMatrix({
      origins: [waypoints[0]],
      destinations: waypoints,
      mode: 'driving'
    }, (err, response) => {
      let maxItem = {value:0,index:0};
      //search for farest waypoint
      _.map(response.json.rows[0].elements, (item, index) => {
        sails.log.info(item);
        if (maxItem.value < item.distance.value) {
          maxItem.value = item.distance.value;
          maxItem.index = index;
        }
      });
      sails.log.verbose("Max distance ", maxItem);
      googleMapsClient.directions(
        {
          origin: waypoints[0],
          destination: waypoints[maxItem.index || 0],
          waypoints: waypoints,
          optimize: true
        },
        (err, response) => {

          if (!err) {
            //Validation rules for response object returned by Google Directions API
            const routeSchema = Joi.object().keys({
              waypoint_order: Joi.array().length(waypoints.length),
              legs: Joi.array().items(
                Joi.object().keys({
                  "distance": Joi.object().keys({
                    "text": Joi.string(),
                    "value": Joi.number()
                  }),
                  "duration": Joi.object().keys({
                    "text": Joi.string(),
                    "value": Joi.number()
                  })
                })
              )
            });

            let {validationError, validationValue} = Joi.validate(
              response.json.routes,
              Joi.array().min(1).items(routeSchema),
              { stripUnknown: true });

              if (!validationError) {
              let waypoint_order = response.json.routes[0].waypoint_order;
              let total_distance = _.sum(response.json.routes[0].legs, (leg) => leg.distance.value);
              let total_time = _.sum(response.json.routes[0].legs, (leg) => leg.duration.value);

              let sorted_waypoints = _.sortBy(waypoints, (item, i) => {
                return waypoint_order.indexOf(i);
              });

              sails.log.verbose({total_distance, total_time, sorted_waypoints});
              sails.log.info('googleMapsClient.directions request finished with success');

              return callback(err, {
                path : sorted_waypoints,
                total_distance,
                total_time
              });
            } else {
              err = 'Error in 3rd party response';
              return callback(err, {error: err});
            }

          } else {
            sails.log.info('googleMapsClient.directions request finished with error');
            sails.log.info("Google Directions API says", {err});
            return callback(err, {error: err.json.error_message});
          }
        }
      );
    });

  }
};
