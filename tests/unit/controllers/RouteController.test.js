var request = require('supertest');
require('should');

describe('RouteController', () => {
  describe('post /route', () => {
    it('POST with valid body should return 200 and token', (done) => {
      request(sails.hooks.http.app)
        .post('/route')
        .send([
          ["22.372081", "114.107877"],
          ["22.284419", "114.159510"],
          ["22.326442", "114.167811"]
        ])
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('token');
          done();
        });
    });

    it('POST with empty body should return 200 and error', (done) => {
      directions.get = (legs, callback) => {
        return callback(null, {});
      }
      request(sails.hooks.http.app)
        .post('/route')
        .send([
        ])
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('error');
          done();
        });
    });

    it('POST with invalid body should return 200 and error', (done) => {
      directions.get = (legs, callback) => {
        return callback(null, {});
      }
      request(sails.hooks.http.app)
        .post('/route')
        .send([
          [12,12,12,12,12],
          [12,12,12,12,12],
          [12,12,12,12,12],
          [12,12,12,12,12]
        ])
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('error');
          done();
        });
    });

  });

  describe('get /route/:token', () => {
    it('without parameters should return 404', (done) => {
      request(sails.hooks.http.app)
        .get('/route')
        .expect(404)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          done();
        });
    });

    it('with fake token should return 200 and error', (done) => {
      request(sails.hooks.http.app)
        .get('/route/25bdc7fc-f1eb-4aa0-a5ae-69035e14f34c')
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('error');
          res.body.error.should.be.eql('Invalid token');
          done();
        });
    });

    it('with not UUID token should return 200 and error', (done) => {
      request(sails.hooks.http.app)
        .get('/route/SOME_TOKEN_STRING')
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('error');
          res.body.error.should.be.eql('Invalid token');
          done();
        });
    });

    it('should be error in case of malformed json', (done) => {
      request(sails.hooks.http.app)
        .post('/route/')
        .send('{"project":{"description":\'test"}}')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          if (err) {
            throw err;
          }
          res.body.should.have.properties('error');
          done();
        });
    });

  });
});
