describe('Route model', () => {

  describe('#get()', () => {

    it('should return error on empty array', (done) => {
      let routeLegs = [];
      Route.get(routeLegs, (err, result) => {
        err.should.be.eql("Wrong input parameters");
        result.should.be.eql({});
        done();
      });
    });

    it('should return token on valid array', (done) => {
      let routeLegs = [
        ["22.372081", "114.107877"],
        ["22.284419", "114.159510"],
        ["22.326442", "114.167811"]
      ];
      directions.get = (legs, callback) => {
        return callback(null, {});
      }
      Route.get(routeLegs, (err, result) => {
        should.not.exist(err);
        result.should.have.properties('token');
        done();
      });
    });

  });

  describe('#getByToken()', () => {

    it('should return error on empty token', (done) => {
      Route.getByToken('', (err, result) => {
        err.should.be.eql("Invalid token");
        result.should.be.eql({});
        done();
      });
    });

    it('should return error on non uuid token', (done) => {
      Route.getByToken('SOME_TOKEN_STRING', (err, result) => {
        err.should.be.eql("Invalid token");
        result.should.be.eql({});
        done();
      });
    });

  });

  describe('#getStatusText()', () => {

    it('should return default text on empty status', (done) => {
      let result = Route.getStatusText(null);
      result.should.be.eql('in progress');
      done();
    });

    it('should return default text on non-exists or 0 status', (done) => {
      let result = Route.getStatusText('');
      result.should.be.eql('in progress');

      result = Route.getStatusText(0);
      result.should.be.eql('in progress');

      result = Route.getStatusText(10);
      result.should.be.eql('in progress');
      done();
    });

    it('should return "success" text on status 1', (done) => {
      let result = Route.getStatusText(1);
      result.should.be.eql('success');
      done();
    });

    it('should return "failure" text on status 2', (done) => {
      let result = Route.getStatusText(2);
      result.should.be.eql('failure');
      done();
    });

  });

});
