var Sails = require('sails'),
  sails;
  require('should');

before(function(done) {

  Sails.lift({
    // configuration for testing purposes
    environment: 'test',
    port: 4000,
    proxyHost: 'http://localhost/',
    proxyPort: 4000,
    explicitHost: false,
    models: {
      connection: 'testMemoryDb'
    },
    log: {
      level: 'error',
      timestamp: false
    },
    hooks: {
      session: false,
      grunt: false
    },
    connections: {
      testMemoryDb: {
        adapter   : 'sails-memory'
      }
    }
  }, function(err, server) {
    sails = server;
    if (err) return done(err);
    // here you can load fixtures, etc.
    done(err, sails);
  });
});

after(function(done) {
  // here you can clear fixtures, etc.
  Sails.lower(done);
});
